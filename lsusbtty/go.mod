module smolltools/lsusbtty

go 1.21.4

require (
	github.com/morgulbrut/color256 v0.0.0-20210301135729-129d0bff979e
	github.com/morgulbrut/smoltools v0.0.0-20240126084512-57f6fb7bd4d2
	go.bug.st/serial.v1 v0.0.0-20191202182710-24a6610f0541
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/stretchr/testify v1.9.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
