package main

import (
	"fmt"
	"time"

	"github.com/inancgumus/screen"
)

func main() {
	type placeholder [3]string

	zero := placeholder{
		"╔═┓",
		"┃ ┃",
		"╚═╝"}
	one := placeholder{
		"═┓ ",
		" ║ ",
		"═┻═"}
	two := placeholder{
		"╔═┓",
		"╔═╝",
		"┗══"}
	three := placeholder{
		"╒═┓",
		" ═╣",
		"╘═┛"}
	four := placeholder{
		"╻ ╻",
		"┗═┫",
		"  ╹"}
	five := placeholder{
		"╔═╸",
		"╚═╗",
		"╘═┛"}
	six := placeholder{
		"╔═╸",
		"╠═╗",
		"┗═┛"}
	seven := placeholder{
		"╒═┓",
		"  ┃",
		"  ╹"}
	eight := placeholder{
		"╔═┓",
		"╠═┫",
		"┗━╝"}
	nine := placeholder{
		"╔═┓",
		"┗═┫",
		" ═┛"}

	sep := placeholder{
		"   ",
		" ╏ ",
		"   "}

	digits := [...]placeholder{zero, one, two, three, four, five, six, seven, eight, nine}

	for {
		screen.Clear()
		screen.MoveTopLeft()

		now := time.Now()
		hour, minute, second := now.Hour(), now.Minute(), now.Second()

		clock := [...]placeholder{digits[hour/10], digits[hour%10], sep, digits[minute/10], digits[minute%10], sep, digits[second/10], digits[second%10]}

		for i := range clock[0] {
			for _, digit := range clock {
				fmt.Print(digit[i], " ")
			}
			fmt.Println()
		}
		time.Sleep(time.Second)

	}
}
