module smolltools/coolingcalc

go 1.21.4

require (
	github.com/google/subcommands v1.2.0
	github.com/morgulbrut/smoltools v0.0.0-20240126084512-57f6fb7bd4d2
)
