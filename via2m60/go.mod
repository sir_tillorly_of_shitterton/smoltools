module smolltools/via2m60

go 1.21.4

require (
	github.com/morgulbrut/color256 v0.0.0-20210301135729-129d0bff979e
	github.com/morgulbrut/smoltools v0.0.0-20240126084512-57f6fb7bd4d2
	github.com/morgulbrut/toml v0.3.1
)
