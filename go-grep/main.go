package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"

	"github.com/morgulbrut/color256"
)

var COLS = []color256.Colr{
	color256.ColRed,
	color256.ColGreen,
	color256.ColYellow,
	color256.ColBlue,
	color256.ColMagenta,
	color256.ColCyan,
	color256.ColOrange,
	color256.ColPink,
}

var root string
var found = 1
var wg sync.WaitGroup

type arrayFlags []string

func (i *arrayFlags) String() string {
	return "my string representation"
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

var query arrayFlags

type keyword struct {
	text  string
	color color256.Colr
}

type result struct {
	root  string
	path  string
	lines []string
}

func isInputFromPipe() bool {
	fmt.Println("Is Pipe?")
	fileInfo, _ := os.Stdin.Stat()
	ret := fileInfo.Mode()&os.ModeCharDevice == 0
	fmt.Println(ret)
	return ret
}

func formatLine(text string, keywords []keyword) string {
	for _, k := range keywords {
		text = strings.Replace(text, k.text, color256.Color(k.color, k.text), -1)
	}
	return strings.TrimSpace(text)
}

func readPipe(keywords []keyword) {
	fmt.Println("Reading from pipe")

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		text := scanner.Text()
		added := false
		for _, k := range keywords {
			if strings.Contains(strings.ToUpper(scanner.Text()), strings.ToUpper(k.text)) {
				found = 0

				if !added {
					fmt.Println(formatLine(text, keywords))
					added = true
				}
			}
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	}
}

func readFile(wg *sync.WaitGroup, path string, keywords []keyword) {
	defer wg.Done()

	file, err := os.Open(path)
	defer file.Close()

	if err != nil {
		return
	}
	var res result

	scanner := bufio.NewScanner(file)
	for i := 1; scanner.Scan(); i++ {
		added := false
		for _, k := range keywords {
			if strings.Contains(strings.ToUpper(scanner.Text()), strings.ToUpper(k.text)) {
				res.root = root
				res.path = path

				if !added {
					res.lines = append(res.lines, fmt.Sprintf("%s\t %s", color256.BgYellow(strconv.Itoa(i)+":"), formatLine(scanner.Text(), keywords)))
				}
				added = true
			}
		}
	}

	if len(res.lines) > 0 {
		color256.PrintBgHiGreen(res.path + ":")
		for _, l := range res.lines {
			fmt.Printf("%s\n", l)
		}
	}

}

func main() {

	flag.Var(&query, "e", "Add keyword to search for")
	flag.Parse()
	root = flag.Arg(0)
	isPipe := isInputFromPipe()

	keywords := []keyword{}
	for i, q := range query {
		keywords = append(keywords, keyword{text: q, color: COLS[i%len(COLS)]})
	}

	if isPipe {
		readPipe(keywords)
		os.Exit(0)
	}

	if root == "" {
		os.Exit(2)
	}

	filepath.Walk(root, func(path string, file os.FileInfo, err error) error {
		if !file.IsDir() {
			wg.Add(1)
			go readFile(&wg, path, keywords)
		}
		return nil
	})
	wg.Wait()
	defer os.Exit(found)
}
