package main

import (
	"bufio"
	"flag"
	"fmt"
	"index/suffixarray"
	"log"
	"os"
	"strings"

	"github.com/morgulbrut/color256"
)

func logo() {
	color256.PrintRandom(" ██████╗  ██████╗ ██████╗ ██████╗")
	color256.PrintRandom("██╔════╝ ██╔═══██╗██╔══██╗██╔══██╗")
	color256.PrintRandom("██║  ███╗██║   ██║██████╔╝██████╔╝")
	color256.PrintRandom("██║   ██║██║   ██║██╔══██╗██╔═══╝")
	color256.PrintRandom("╚██████╔╝╚██████╔╝██║  ██║██║")
	color256.PrintRandom(" ╚═════╝  ╚═════╝ ╚═╝  ╚═╝╚═╝")
}

type arrayFlags []string

func (i *arrayFlags) String() string {
	var sb strings.Builder
	for _, f := range *i {
		sb.WriteString(" " + f)
	}
	return sb.String()
}

func (i *arrayFlags) FilenameString() string {
	return strip(i.String())
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func strip(s string) string {
	var result strings.Builder
	for i := 0; i < len(s); i++ {
		b := s[i]
		if ('a' <= b && b <= 'z') ||
			('A' <= b && b <= 'Z') ||
			('0' <= b && b <= '9') {
			result.WriteByte(b)
		} else if b == ' ' {
			result.WriteByte('_')
		}
	}
	return result.String()
}

func main() {
	var querries arrayFlags
	var beginSegment string
	var endSegment string
	var path string
	var export bool

	logo()
	flag.Var(&querries, "e", "Some description for this param.")
	flag.StringVar(&beginSegment, "start", "GPI 1=1", "Begin of a segment")
	flag.StringVar(&endSegment, "stop", "registers as hex:", "End of a segment")
	// flag.StringVar(&path, "f", "example.log", "Path to File")
	flag.BoolVar(&export, "s", false, "Export founds to file")
	flag.Parse()

	if len(os.Args) >= 2 {
		path = os.Args[len(os.Args)-1]
	}

	color256.PrintHiMagenta("Reading logfile: %s", path)
	trs := scanLog(path, beginSegment, endSegment)
	color256.PrintHiMagenta("Searching logfile for%s", querries.String())
	res := searchLog(trs, querries)

	if export {
		ofn := fmt.Sprintf("%s%s", path, querries.FilenameString())
		f, err := os.Create(ofn)
		check(err)
		color256.PrintHiMagenta("Writing ouput")
		for _, r := range res {
			f.WriteString(r)
			f.WriteString("\n\n\n")
		}
		defer f.Close()
	} else {
		for i, r := range res {
			color256.PrintHiGreen("--------- Result %d ---------", i+1)
			print(r)
		}
	}

	color256.PrintHiGreen("Found %d segments", len(res))
}

func searchLog(tagreads []string, querries []string) (results []string) {
	var res []string

	for _, r := range tagreads {
		for _, q := range querries {
			if strings.Contains(r, q) {
				res = append(res, r)
			}
		}
	}
	return res
}

func scanLog(fn string, bs string, es string) (tagReads []string) {
	var tr strings.Builder
	var trs []string

	file, err := os.Open(fn)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		index := suffixarray.New([]byte(line))

		if len(index.Lookup([]byte(bs), -1)) != 0 {
			tr.Reset()
			tr.WriteString(line + "\n")
		} else if len(index.Lookup([]byte(es), -1)) != 0 {
			tr.WriteString(line + "\n")
			trs = append(trs, tr.String())
			tr.Reset()
		} else {
			tr.WriteString(line + "\n")
		}
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
	return trs
}
