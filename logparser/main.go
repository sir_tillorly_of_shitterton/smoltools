package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strings"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Println("Please provide a search word.")
		return
	}

	rx := regexp.MustCompile(`[^a-z]+`)

	query := strings.ToLower(args[0])

	in := bufio.NewScanner(os.Stdin)
	in.Split(bufio.ScanWords)

	words := make(map[string]bool)
	for in.Scan() {
		word := strings.ToLower(in.Text())
		word = rx.ReplaceAllString(word, "")

		if len(word) > 2 {
			words[word] = true
		}
	}

	if words[query] {
		fmt.Println("The word", query, "was found.")
	} else {
		fmt.Println("The word", query, "was not found.")
	}

	for word := range words {
		fmt.Println(word)
	}
}

// func main() {
// 	in := bufio.NewScanner(os.Stdin)

// 	var lines int

// 	for in.Scan() {
// 		fmt.Println(in.Text())
// 		lines++
// 	}

// 	fmt.Println("Number of lines:", lines)

// 	if err := in.Err(); err != nil {
// 		fmt.Fprintln(os.Stderr, "Error:", err)
// 	}
// }
