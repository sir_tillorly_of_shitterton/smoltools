import os
import subprocess
from colorama import init, Fore, Style
import argparse

# Initialize colorama
init(autoreset=True)


def run_command(command, cwd):
    result = subprocess.run(command, shell=True, cwd=cwd,
                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if result.returncode == 0:
        print(Fore.GREEN + f"Success: {command} in {cwd}")
    else:
        print(Fore.RED + f"Error: {command} in {cwd}")
        print(Fore.RED + result.stderr.decode())


def main():
    parser = argparse.ArgumentParser(
        description="Build all Go projects in subdirectories.")
    parser.add_argument('--install', action='store_true',
                        help='Install the built binaries')
    parser.add_argument('--init', default=True, action='store_true',
                        help='Initialize the Go modules')
    parser.add_argument('--build', action='store_true',
                        help='Build the Go projects')

    args = parser.parse_args()
    base_dir = os.path.dirname(os.path.abspath(__file__))
    for directory in os.listdir(base_dir):
        dir_path = os.path.join(base_dir, directory)
        if os.path.isdir(dir_path) and os.path.exists(os.path.join(dir_path, "main.go")):
            print(Style.BRIGHT + Fore.CYAN +
                  f"Processing directory: {directory}")
            if args.init:
                run_command(f"go mod init smolltools/{directory}", dir_path)
                run_command("go mod tidy", dir_path)
            if args.build:
                run_command("go build", dir_path)
            if args.install:
                run_command("go install", dir_path)


if __name__ == "__main__":
    main()
