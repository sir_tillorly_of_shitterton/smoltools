module syslgo

go 1.21.4

require (
	github.com/acarl005/stripansi v0.0.0-20180116102854-5a71ef0e047d
	github.com/morgulbrut/color256 v0.0.0-20210301135729-129d0bff979e
	gopkg.in/mcuadros/go-syslog.v2 v2.3.0
)

require gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
